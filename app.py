from flask import Flask, render_template, request
from processing import load_models, predict1, predict2


app = Flask(__name__)

# для того, чтобы наше приложение использовалось Flask нужно добавить декоратор

@app.route('/', methods=['get','post'])  # по какому пути будет доступна эта страница (127.0.0.1:5000 + '/' = 127.0.0.1:5000/)
def main():
    model1, scaler_x1, scaler_y1, model2, scaler_x2, scaler_y2 = load_models()
    message = ""

    if request.method == "POST":
        matrix = request.form.get("matrix")
        dens = request.form.get("dens")
        mod_up = request.form.get("mod_up")
        col_otverd = request.form.get("col_otverd")
        epox_gr = request.form.get("epox_gr")
        t_svp = request.form.get("t_svp")
        poverh_plot = request.form.get("poverh_plot")
        # mod_up_rast = float(mod_up_rast)
        # proch_rast = float(proch_rast)
        smola = request.form.get("smola")
        ugol_nash = request.form.get("ugol_nash")
        shag_nash = request.form.get("shag_nash")
        plot_nash = request.form.get("plot_nash")

        try:
            matrix = float(matrix)
            dens = float(dens)
            mod_up = float(mod_up)
            col_otverd = float(col_otverd)
            epox_gr = float(epox_gr)
            t_svp = float(t_svp)
            poverh_plot = float(poverh_plot)
            smola = float(smola)
            ugol_nash = float(ugol_nash)
            shag_nash = float(shag_nash)
            plot_nash = float(plot_nash)

            list_for_models = [matrix, dens, mod_up, col_otverd, epox_gr,
                               t_svp, poverh_plot, smola, ugol_nash, shag_nash, plot_nash]


            mod_up_rast = predict1(scaler_x1.transform([list_for_models]), model1)
            print(mod_up_rast)
            mod_up_rast = scaler_y1.inverse_transform([mod_up_rast])
            print(mod_up_rast)


            proch_rast = predict2(scaler_x2.transform([list_for_models]), model2)
            print(proch_rast)
            proch_rast = scaler_y2.inverse_transform([proch_rast])
            print(proch_rast)

            message = (f"Модуль упругости при растяжении равен: {mod_up_rast[0][0].round(1)} ГПа. \n"
                       f"Прочность при растяжении равна: {proch_rast[0][0].round(1)} МПа.")
            print(message)


        except:
            message = f"Вы ввели некорректное значение в поле. (введите число)"


    return render_template("index.html", message=message)


#app.run()
