import pickle


def load_models(model1_path="models/model1.pkl",
                scaler_x1_path="models/std_scaler_x1.pkl",
                scaler_y1_path="models/std_scaler_y1.pkl",
                model2_path="models/model2.pkl",
                scaler_x2_path="models/std_scaler_x2.pkl",
                scaler_y2_path="models/std_scaler_y2.pkl"):
    with open(model1_path, "rb") as f:
        model1 = pickle.load(f)
    with open(scaler_x1_path, "rb") as f:
        scaler_x1 = pickle.load(f)
    with open(scaler_y1_path, "rb") as f:
        scaler_y1 = pickle.load(f)

    with open(model2_path, "rb") as f:
        model2 = pickle.load(f)
    with open(scaler_x2_path, "rb") as f:
        scaler_x2 = pickle.load(f)
    with open(scaler_y2_path, "rb") as f:
        scaler_y2 = pickle.load(f)

    return model1, scaler_x1, scaler_y1, model2, scaler_x2, scaler_y2


def predict1(list_for_models, model1):
    mod_up_rast = model1.predict(list_for_models)
    print(mod_up_rast)
    return mod_up_rast


def predict2(list_for_models, model2):
    proch_rast = model2.predict(list_for_models)
    print(proch_rast)
    return proch_rast
